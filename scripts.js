function createNewUser() {
    let userFirstName = prompt("What is your name?");
    let userLastName = prompt("What is your last name?");

    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,

        getLogin: function () {
            return this["firstName"].toLowerCase().slice(0,1) + this["lastName"].toLowerCase();
        },

        setFirstName(newValue) {
            Object.defineProperty(this, "firstName", {
                value: newValue,
                writable: false,
            });
        },

        setLastName(newValue) {
            Object.defineProperty(this, "lastName", {
                value: newValue,
                writable: false,
            });
        },
    };

    Object.defineProperty(newUser, "firstName", {
        configurable: true,
        writable: false,
    });

    Object.defineProperty(newUser, "lastName", {
        configurable: true,
        writable: false,
    });

    return newUser;
}

let user = createNewUser();

console.log(user.getLogin());
